// initialises the initial count
let count = 0;

let span = document.getElementById('value');
console.log(span);



function increaseCount() {
    count++;
    span.innerHTML = count;
    if (count > 0)
        span.style.color = 'green';
}

function decreaseCount() {
    count--;
    span.innerHTML = count;
    if (count < 0)
        span.style.color = 'red';
}

function resetCount() {
    count = 0;
    span.innerHTML = count;
    if (count == 0)
        span.style.color = 'black';
}